package leetcode.lesson2;

class Solution {
    public static int removeElement(int[] nums, int val) {
        int k = 0;
        for (int i=0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[k] = nums[i];
                k++;
            }
        }
        return k;
    }

    public static void main(String[] args) {
        int[] a = {3,2,2,3,2};
        System.out.println(removeElement(a, 3));
    }
}