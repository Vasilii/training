package leetcode.interviews;

public class InterviewBarclays {
    public static void main(String[] args) {
        Operand five = new Operand(5);
        Operand seven = new Operand(7);
        Operand eight = new Operand(8);

        Operation multiplication = new Multiplication(seven, eight);
        Operation subtraction = new Subtraction(five, multiplication);

        System.out.println("Result: " + subtraction.evaluate());
    }
}

// Общий интерфейс для операндов и операций
interface Expression {
    int evaluate();
}

// Класс для представления операндов
class Operand implements Expression {
    private int value;

    public Operand(int value) {
        this.value = value;
    }

    @Override
    public int evaluate() {
        return value;
    }
}

// Класс для представления операций
abstract class Operation implements Expression {
    protected Expression leftOperand;
    protected Expression rightOperand;

    public Operation(Expression leftOperand, Expression rightOperand) {
        this.leftOperand = leftOperand;
        this.rightOperand = rightOperand;
    }
}

// Класс для представления операции умножения
class Multiplication extends Operation {
    public Multiplication(Expression leftOperand, Expression rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public int evaluate() {
        return leftOperand.evaluate() * rightOperand.evaluate();
    }
}

// Класс для представления операции вычитания
class Subtraction extends Operation {
    public Subtraction(Expression leftOperand, Expression rightOperand) {
        super(leftOperand, rightOperand);
    }

    @Override
    public int evaluate() {
        return leftOperand.evaluate() - rightOperand.evaluate();
    }
}